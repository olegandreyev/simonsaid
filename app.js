$(function () {
    var combinations = [];
    var difficult = 3;
    var gameObject = $('.container-box div');

    function startGame(difficult) {


        function startRepeat() {
            var step = 600 / difficult;
            $('#progressBar div').css('width', '0px');
            $('#progressBar div').css('background', 'green');

            var count = 0;
            gameObject.on('mousemove', function () {
                $(this).css('opacity', 1)
            })
            gameObject.on('mouseleave', function () {
                $(this).css('opacity', 0.2)
            })
            gameObject.on('click', function () {

                if (combinations[count] == $(this).data('select')) {
                    $('#progressBar div').animate({width: "+=" + step + 'px'}, function () {
                        count++;
                        if (count === combinations.length) {
                            combinations = [];
                            alert('Next Round...!')
                            SimonGame(++difficult, startRepeat)
                        }
                    })

                } else {
                    alert('Game Over')
                    var newGame = confirm('Start new game?');
                    $('#progressBar div').css('background', 'red')
                    combinations = [];
                    difficult = 3;
                    if (newGame) {
                        SimonGame(difficult, startRepeat)
                    } else {
                        gameObject.off('click')
                    }
                    return;
                }
            })
        }


        function SimonGame(difficult, callBack) {
            var step = 600 / difficult;
            $('#progressBar div').css('width', '0px');
            $('#progressBar div').css('background', 'yellow');
            gameObject.off('click')
            gameObject.off('mousemove')
            gameObject.off('mouseleave')
            gameObject.css('opacity', '0.2')


            function countDown(count) {
                var index = Math.floor(Math.random() * 4);

                setTimeout(function () {
                    $('.container-box div[data-select=' + index + ']').css('opacity', '1');

                    setTimeout(function () {
                        $('.container-box div[data-select=' + index + ']').css('opacity', '0.2');
                        combinations.push(index);
                        if (count > 1) {
                            count--;
                            countDown(count)
                            $('#progressBar div').animate({width: "+=" + step + 'px'});

                        } else {
                            $('#progressBar div').animate({width: "+=" + step + 'px'}, function () {
                                alert('REPEAT');
                                callBack();
                            })

                        }

                    }, 1000)

                }, 1000)

            }

            countDown(difficult);
        }

        SimonGame(difficult, startRepeat)
    }

    startGame(difficult)

})
